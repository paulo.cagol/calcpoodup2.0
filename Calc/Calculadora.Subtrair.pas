unit Calculadora.Subtrair;

interface

uses
  Calculadora.Interfaces, Calculadora.Operacoes, System.Generics.Collections,
  System.SysUtils;

type
  TSubtrair = class sealed (TOperacoes)
  public
    constructor Create(var Value : TList<Double>);
    destructor Destroy; override;

    class function New(var Value : TList<Double>): iOperacoes;

    function Executar: String; override;
  end;

implementation

{ TSubtrair }

constructor TSubtrair.Create(var Value : TList<Double>);
begin
  FLista := Value;
end;

destructor TSubtrair.Destroy;
begin

  inherited;
end;

function TSubtrair.Executar: String;
var
  I: Integer;
begin
  Result := FLista[0].ToString;
  for I := 1 to FLista.Count - 1 do
    Result := (Result.ToDouble - FLista[I]).ToString;
  DisplayTotal(Result);

  inherited;
end;

class function TSubtrair.New(var Value : TList<Double>): iOperacoes;
begin
  Result := Self.Create(Value);
end;


end.
