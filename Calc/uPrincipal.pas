unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Calculadora.Interfaces,
  Calculadora.Helpers,
  Classe.Calculadora;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label1: TLabel;
    Edit3: TEdit;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    FCalculadora: iCalculadora;
    procedure ExibirResultado(Value: String);
    procedure ExibirLabel(Value: String);

  public
    { Public declarations }
    property Calculadora : iCalculadora read FCalculadora write FCalculadora;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  Calculadora
    .Add(Edit1.Text)
    .Add(Edit2.Text)
    .Soma
      .Display
        .Resultado(ExibirLabel)
      .EndDisplay
    .Executar;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Calculadora
    .Add(Edit1.Text)
    .Add(Edit2.Text)
    .Subtrair.Executar;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Calculadora
    .Add(Edit1.Text)
    .Add(Edit2.Text)
    .Dividir.Executar;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Calculadora
    .Add(Edit1.Text)
    .Add(Edit2.Text)
    .Multiplicar.Executar;
end;

procedure TForm1.ExibirLabel(Value: String);
begin
  Label2.Caption := Value;
end;

procedure TForm1.ExibirResultado(Value: String);
begin
  Edit3.Text := Value;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Calculadora := TCalculadora.New
                  .Display
                    .Resultado(ExibirResultado)
                  .EndDisplay;
end;

end.
