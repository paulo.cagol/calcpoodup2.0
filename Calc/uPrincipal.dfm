object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 227
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 181
    Top = 76
    Width = 20
    Height = 33
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 32
    Top = 168
    Width = 318
    Height = 35
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 32
    Top = 58
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 32
    Top = 85
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 32
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Somar'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 113
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Subtratir'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 194
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Dividir'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 275
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Multiplicar'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Edit3: TEdit
    Left = 229
    Top = 85
    Width = 121
    Height = 21
    TabOrder = 6
  end
end
