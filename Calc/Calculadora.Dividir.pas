unit Calculadora.Dividir;

interface

uses
  Calculadora.Interfaces, Calculadora.Operacoes, System.Generics.Collections;

type
  TDividir = class sealed (TOperacoes)
  public
    constructor Create(var Value : TList<Double>);
    destructor Destroy; override;

    class function New(var Value : TList<Double>): iOperacoes;

    function Executar: String; override;
  end;

implementation

uses
  System.SysUtils;

{ TDividir }

constructor TDividir.Create(var Value : TList<Double>);
begin
  FLista := Value;
end;

destructor TDividir.Destroy;
begin

  inherited;
end;

function TDividir.Executar: String;
var
  I: Integer;
begin
  Result := FLista[0].ToString;
  for I := 1 to FLista.Count - 1 do
    Result := (Result.ToDouble / FLista[I]).ToString;
  DisplayTotal(Result);

  inherited;
end;

class function TDividir.New(var Value : TList<Double>): iOperacoes;
begin
  Result := Self.Create(Value);
end;


end.
