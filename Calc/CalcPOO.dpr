program CalcPOO;

uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {Form1},
  Classe.Calculadora in 'Classe.Calculadora.pas',
  Calculadora.Interfaces in 'Calculadora.Interfaces.pas',
  Calculadora.Helpers in 'Calculadora.Helpers.pas',
  Calculadora.Soma in 'Calculadora.Soma.pas',
  Calculadora.Subtrair in 'Calculadora.Subtrair.pas',
  Calculadora.Dividir in 'Calculadora.Dividir.pas',
  Calculadora.Multiplicar in 'Calculadora.Multiplicar.pas',
  Calculadora.Operacoes in 'Calculadora.Operacoes.pas',
  Calculadora.Eventos in 'Calculadora.Eventos.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
