unit Calculadora.Interfaces;

interface

uses
  Calculadora.Eventos;

type
  iCalculadora = interface;
  iOperacoesDisplay = interface;

  iOperacoes = interface
    ['{4A726B0B-6127-476E-9634-BA527C1E0B56}']

    function Executar: String;
    function Display : iOperacoesDisplay;
  end;

  iOperacoesDisplay = interface
    ['{FDA9C09C-C7EA-4218-8413-4250055579B8}']
    function Resultado(Value: TEventDisplayTotal): iOperacoesDisplay;
    function EndDisplay: iOperacoes;
  end;

  iCalculadoraDisplay = interface
    ['{15AF0C74-7961-4418-A66D-BFE8CC5A0450}']
    function Resultado(Value: TEventDisplayTotal): iCalculadoraDisplay;
    function EndDisplay: iCalculadora;
  end;

  iCalculadora = interface
    ['{AD919BAD-6487-4C07-A2FF-590173D9DBD1}']

    function Add(Value: String  ): iCalculadora; overload;
    function Add(Value: integer ): iCalculadora; overload;
    function Add(Value: Currency): iCalculadora; overload;


    function Soma: iOperacoes;
    function Subtrair: iOperacoes;
    function Dividir: iOperacoes;
    function Multiplicar: iOperacoes;
    function Display: iCalculadoraDisplay;
  end;

implementation

end.
