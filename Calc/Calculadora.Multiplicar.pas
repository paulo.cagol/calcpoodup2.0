unit Calculadora.Multiplicar;

interface

uses
  Calculadora.Operacoes, Calculadora.Interfaces, System.Generics.Collections,
  System.SysUtils;

type
  TMultiplicar = class sealed(TOperacoes)
  public
    constructor Create(var Value: TList<Double>);
    destructor Destroy; override;

    class function New(var Value: TList<Double>): iOperacoes;

    function Executar: String; override;
  end;

implementation

{ TMultiplicar }

constructor TMultiplicar.Create(var Value: TList<Double>);
begin
  FLista := Value;
end;

destructor TMultiplicar.Destroy;
begin

  inherited;
end;

function TMultiplicar.Executar: String;
var
  I: Integer;
begin
  Result := FLista[0].ToString;
  for I := 1 to FLista.Count - 1 do
    Result := (Result.ToDouble * FLista[I]).ToString;
  DisplayTotal(Result);

  inherited;
end;

class function TMultiplicar.New(var Value: TList<Double>): iOperacoes;
begin
  Result := Self.Create(Value);
end;

end.
