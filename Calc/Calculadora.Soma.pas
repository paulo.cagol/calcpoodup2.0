unit Calculadora.Soma;

interface

uses
  Calculadora.Interfaces, Calculadora.Operacoes, System.Generics.Collections,
  System.SysUtils;

type
  TSoma = class sealed (TOperacoes)
  public
    constructor Create(var Value : TList<Double>);
    destructor Destroy; override;

    class function New(var Value : TList<Double>): iOperacoes;

    function Executar: String; override;
  end;

implementation

{ TSoma }

constructor TSoma.Create(var Value : TList<Double>);
begin
  FLista := Value;
end;

destructor TSoma.Destroy;
begin

  inherited;
end;

function TSoma.Executar: String;
var
  I: Integer;
begin
  Result := FLista[0].ToString;
  for I := 1 to FLista.Count - 1 do
    Result := (Result.ToDouble + FLista[I]).ToString;
  DisplayTotal(Result);

  inherited;
end;

class function TSoma.New(var Value : TList<Double>): iOperacoes;
begin
  Result := Self.Create(Value);
end;

end.
