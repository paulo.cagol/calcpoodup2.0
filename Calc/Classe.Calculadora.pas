unit Classe.Calculadora;

interface

uses
  Calculadora.Interfaces, Calculadora.Soma, Calculadora.Subtrair, Calculadora.Dividir,
  Calculadora.Multiplicar, System.Generics.Collections, Calculadora.Eventos;

type
  TCalculadora = class(TInterfacedObject, iCalculadora, iCalculadoraDisplay)
  private
    FLista : TList<Double>;
    FEventDisplayTotal: TEventDisplayTotal;
  public
    constructor Create;
    destructor Destroy; override;
    class function New: iCalculadora;

    function Add(Value: String  ): iCalculadora; overload;
    function Add(Value: integer ): iCalculadora; overload;
    function Add(Value: Currency): iCalculadora; overload;

    function Soma: iOperacoes;
    function Subtrair: iOperacoes;
    function Dividir: iOperacoes;
    function Multiplicar: iOperacoes;

    function Display: iCalculadoraDisplay;
    function Resultado(Value: TEventDisplayTotal): iCalculadoraDisplay;
    function EndDisplay: iCalculadora;
  end;

implementation

uses
  System.SysUtils;

{ TCalculadora }

function TCalculadora.Add(Value: String): iCalculadora;
begin
  Result := Self;
  FLista.Add(Value.ToDouble);
end;

function TCalculadora.Add(Value: integer): iCalculadora;
begin
  Result := Self;
  FLista.Add(Value.ToDouble);
end;

function TCalculadora.Add(Value: Currency): iCalculadora;
begin
  Result := Self;
  FLista.Add(Value);
end;

constructor TCalculadora.Create;
begin
  FLista := TList<Double>.Create;
end;

destructor TCalculadora.Destroy;
begin
  FLista.Free;
  inherited;
end;

function TCalculadora.Soma: iOperacoes;
begin
  Result := TSoma.New(FLista).Display.Resultado(FEventDisplayTotal).EndDisplay;
end;

function TCalculadora.Display: iCalculadoraDisplay;
begin
  Result := Self;
end;

function TCalculadora.Dividir: iOperacoes;
begin
  Result := TDividir.New(FLista).Display.Resultado(FEventDisplayTotal).EndDisplay;
end;

function TCalculadora.EndDisplay: iCalculadora;
begin
  Result := Self;
end;

function TCalculadora.Multiplicar: iOperacoes;
begin
  Result := TMultiplicar.New(FLista).Display.Resultado(FEventDisplayTotal).EndDisplay;
end;

class function TCalculadora.New: iCalculadora;
begin
  Result := Self.Create;
end;

function TCalculadora.Resultado(Value: TEventDisplayTotal): iCalculadoraDisplay;
begin
  Result := Self;
  FEventDisplayTotal := Value;
end;

function TCalculadora.Subtrair: iOperacoes;
begin
  Result := TSubtrair.New(FLista).Display.Resultado(FEventDisplayTotal).EndDisplay;
end;

end.
